<?php

use Kirby\Http\Response;

/**
 * Lists directories in given path
 *
 * @param string $path
 * @return array
 */
function getDirectories(string $path): array
{
    $directories = [];
    $items = scandir($path);
    foreach ($items as $item) {
        if ($item == ".." || $item == ".") {
            continue;
        }
        if (is_dir($path . "/" . $item)) {
            $directories[] = $item;
        }
    }
    return $directories;
}

/**
 * Lists the installed plugins
 *
 * @param $path
 * @return array
 */
function getInstalledPlugins($path)
{
    $plugins = [];

    foreach ($path as $value) {
        $file = __DIR__ . "/../" . $value . "/composer.json";
        $content = "";

        if (file_exists($file)) {
            $content = json_decode(file_get_contents($file));
        }

        array_push($plugins, [
            "name" => $content ? $content->name : "",
            "description" => $content ? $content->description : "",
            "path" => $value,
            "version" => $content ? $content->version : "",
        ]);
    }

    return $plugins;
}

/**
 * Lists the installed node modules
 *
 * @param $file
 * @return array
 */
function getInstalledNodeModules($file)
{
    $modules = [
        "dependencies" => [],
        "devDependencies" => []
    ];

    if (file_exists($file)) {
        $content = json_decode(file_get_contents($file), true);

        $modules["dependencies"] = $content["dependencies"] ?: [];
        $modules["devDependencies"] = $content["devDependencies"] ?: [];
    }

    return $modules;
}

Kirby::plugin("gb-kirby/info-api", [
    "routes" => function ($kirby) {
        return [
            [
                "pattern" => "/gb/info",
                "action" => function () use (&$kirby) {
                    $path = getDirectories(__DIR__ . "/../");
                    $plugins = getInstalledPlugins($path);

                    $packageJson = __DIR__ . "/../../../package.json";
                    $modules = getInstalledNodeModules($packageJson);

                    $array = [
                        "version" => $kirby->version(),
                        "php" => phpversion(),
                        "plugins" => $plugins,
                        "node_modules" => $modules
                    ];

                    return new Response(json_encode($array), "application/json");
                }
            ]
        ];
    }
]);

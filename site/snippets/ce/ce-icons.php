<div class="ce-icons" id="<?= $data->slug() ?>">
  <?php if($data->headline()->isNotEmpty()): ?>
    <div class="ce-icons__title">
      <h3><?= $data->headline()->kti() ?></h3>
    </div>
  <?php endif ?>
  <ul class="ce-icons__list">
    <?php foreach($data->list()->toStructure() as $item): ?>
      <li class="ce-icons__item">
        <?php if($icon = $item->icon()->toFile()): ?>
          <div class="ce-icons__item__icon">
            <img src="<?= $icon->url() ?>" alt>
          </div>
        <?php endif ?>
        <?= $item->text()->kti() ?>
      </li>
    <?php endforeach ?>
  </ul>
</div>

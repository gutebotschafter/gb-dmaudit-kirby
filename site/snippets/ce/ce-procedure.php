<div class="ce-procedure" id="<?= $data->slug() ?>">
  <?php if($data->headline()->isNotEmpty()): ?>
    <div class="ce-procedure__title">
      <h3><?= $data->headline()->kti() ?></h3>
    </div>
  <?php endif ?>
  <ol class="ce-procedure__list">
    <?php foreach($data->list()->toStructure() as $item): ?>
      <li class="ce-procedure__list__item">
        <?= $item->text()->kti() ?>
      </li>
    <?php endforeach ?>
  </ol>
</div>

<aside class="page-contact" id="page-contact">
  <div class="page-contact__section">
    <h2>Jetzt einen <strong>Termin</strong> vereinbaren!</h2>
    <div class="page-contact__info">
      <?php if($site->ctaImage()->toFile()): ?>
        <div class="page-contact__info__image">
          <img src="<?= $site->ctaImage()->toFile()->thumb(['width' => 480, 'height' => 480, 'crop' => true, 'quality' => 90])->url() ?>" alt="<?= $site->ctaImage()->toFile()->alt() ?>">
        </div>
      <?php endif ?>
      <div class="page-contact__info__text">
        <?= $site->cta()->kirbytext() ?>
      </div>
    </div>
  </div>
  <div class="page-contact__section" id="page-contact-form">

    <?php if($success): ?>
      <p><strong><em>Eine E-Mail mit Ihrer Anfrage wurde erfolgreich versendet.</em></strong></p>
      <p>Vielen Dank für Ihr Interesse an unserem <strong>Online-Marketing-Audit.</strong> Wir melden uns umgehend bei Ihnen.</p>
    <?php else: ?>

      <?php if (isset($alert['error'])): ?>
        <p><?= $alert['error'] ?></p>
      <?php endif ?>

      <form class="form" method="post" action="<?= $page->url() ?>#page-contact-form">
        <div class="form__field">
          <label class="form__field__label" for="name">Name*</label>
          <input class="form__field__text" type="text" id="name" name="name" value="<?= $data['name'] ?? '' ?>" required>
          <?= isset($alert['name']) ? '<span class="form__field__error">' . html($alert['name']) . '</span>' : '' ?>
        </div>
        <div class="form__field">
          <label class="form__field__label" for="email">E-Mail*</label>
          <input class="form__field__text" type="email" id="email" name="email" required>
          <?= isset($alert['email']) ? '<span class="form__field__error">' . html($alert['email']) . '</span>' : '' ?>
        </div>
        <div class="form__field">
          <label class="form__field__label" for="url">Webseite*</label>
          <input class="form__field__text" type="text" id="url" name="url" required>
          <?= isset($alert['url']) ? '<span class="form__field__error">' . html($alert['url']) . '</span>' : '' ?>
        </div>
        <div class="form__field">
          <label class="form__field__label" for="phone">Telefon</label>
          <input class="form__field__text" type="text" id="phone" name="phone">
          <?= isset($alert['phone']) ? '<span class="form__field__error">' . html($alert['phone']) . '</span>' : '' ?>
        </div>
        <div class="form__field form__field--select">
          <label class="form__field__label" for="topic">Ihr gewünschter Themen-Schwerpunkt*</label>
          <select class="form__field__select" id="topic" name="topic" required>
            <option disabled selected value="select">Bitte Auswählen</option>
            <option value="Suchmaschinenoptimierung (SEO">Suchmaschinenoptimierung (SEO)</option>
            <option value="Online-Werbeanzeigen (Paid)">Online-Werbeanzeigen (Paid)</option>
            <option value="Social Media">Social Media</option>
            <option value="E-Mail-Marketing">E-Mail-Marketing</option>
          </select>
          <?= isset($alert['topic']) ? '<span class="form__field__error">' . html($alert['topic']) . '</span>' : '' ?>
        </div>
        <div class="form__field form__field--honeypot">
          <label class="form__field__label" for="website">Webseite</label>
          <input class="form__field__text" type="website" id="website" name="website">
        </div>
        <div class="form__field">
          <input class="form__field__btn" type="submit" name="submit" value="Absenden">
        </div>
      </form>

    <?php endif ?>
  </div>
</aside>

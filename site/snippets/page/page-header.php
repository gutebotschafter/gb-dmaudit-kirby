<header class="page-header">
  <?php if($page->isHomePage()): ?>
    <h1 class="page-header__image">
      <img src="<?= url('assets/images/title.svg') ?>" alt="<?= $site->title()->html() ?> – <?= $site->claim()->html() ?>">
    </h1>
  <?php else: ?>
    <h1 class="page-header__title"><?= $page->title()->html() ?></h1>
  <?php endif ?>
</header>

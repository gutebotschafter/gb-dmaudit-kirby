<p>Auf der DM Audit Webseite ist eine neue Kontaktanfrage hinterlassen worden.</p>
<p>Folgende Kontaktdaten wurden hinterlassen:</p>

<ul>
  <li>Name: <?= $name ?></li>
  <li>E-Mail: <?= $email ?></li>
  <li>Telefon: <?= $phone ?></li>
  <li>Webseite: <?= $url ?></li>
  <li>Themenschwerpunkt: <?= $topic ?></li>
</ul>

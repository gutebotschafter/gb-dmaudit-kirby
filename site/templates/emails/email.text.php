Auf der DM Audit Webseite ist eine neue Kontaktanfrage hinterlassen worden. Folgende Kontaktdaten wurden hinterlassen:

Name: <?= $name ?>

E-Mail: <?= $email ?>

Telefon: <?= $phone ?>

Webseite: <?= $url ?>

Themenschwerpunkt: <?= $topic ?>
